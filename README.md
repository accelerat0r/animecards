# animecards
This is a collection of scripts to mimic ShareX in GNU/Linux. Rather than using
an abomination like "sharenix", I have tried to make minimal scripts using
small terminal programs, interfaced via dmenu.

The scripts can be found in the `scripts` folder. The `img` and `snd` folder
hold onto created files so that they can be added to anki cards.

i2clip deletes its temporary image, since the clipboard image can be pasted
directly into anki.

For info on how to learn Japanese with these programs, visit:
https://itazuraneko.neocities.org
https://animecards.site