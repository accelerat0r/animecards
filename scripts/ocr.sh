#!/bin/bash
# Screenshot part of the screen and feed that screenshot to tesseract, then put
# tesseract's text output into the clipboard.

# configure your menu command here
_dmenu_command="dmenu"

# get the tesseract psm
choices="hb [horizontal-block] [default]\nsw [single-world]\nsc [single-char]";
chosen=$(echo -e "$choices" | "$_dmenu_command" -i -p "document layout: ");

code=6
case $chosen in
    "hb [horizontal-block] [default]")   code=6  ;;
    "sw [single-world]")                 code=8  ;;
    "sc [single-char]")                  code=10 ;;
esac

# screenshot the selection
tmpdir=$(mktemp -d);
infilename=$(mktemp -p $tmpdir --suffix=.png);
outfilename=$(mktemp -u -p $tmpdir); 

trap "rm -rf ${tmpdir}; exit" INT TERM EXIT; 
scrot -s -o -f "${infilename}";

# perform ocr and put the result in the clipboard.
# for some reason outputting to stdout and piping into xclip does not seem to
# reliably work, so we create an output file and read that into xclip.
tesseract "${infilename}" "${outfilename}" -l jpn --psm $code;

# tesseract has a nice "feature" where it appends .txt to the outfile, even
# if you already explicitly included it.
xclip -selection clipboard -i ${outfilename}.txt;

exit 0
