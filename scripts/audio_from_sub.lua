utils = require "mp.utils"
msg = require "mp.msg"
assdraw = require "mp.assdraw"

-- ========================================================================== --
--                                                                            --
--                               CONFIGURATION                                --
--                                                                            --
-- ========================================================================== --

-- The name of the field for the sentence in your anki deck: the selected subtitle will go here.
sentence_field_name = "Sentence"
-- The name of the field for the picture in your anki deck: the screenshot will go here.
picture_field_name = "Picture"
-- The name of the field for the audio in your anki deck: the recorded audio will go here.
audio_field_name = "Audio"

-- ========================================================================== --
--                                                                            --
--                                   GLOBALS                                  --
--                                                                            --
-- ========================================================================== --

-- how many later subs to include in the card
forward_offset = 0
-- how many previous subs to include in the card
previous_offset = 0

function anki()
   local args = {
      "curl", "localhost:8765", "-X", "POST",
      "-d", "{\"action\": \"findNotes\", \"version\": 6, \"params\": { \"query\": \"deck:test\"} }",
      "-o", "-"
   }

   local audio_value = "audio append"
   local sentence_value = "sentence append"
   local picture_value = "picture append"

   local res = utils.subprocess({ args = args })
   require 'pl.pretty'.dump(res)

   if (res["stdout"] ~= 0) then
     res_start, res_end = string.find(res["stdout"], "%[%d+%], \"error\":")
     local ac_result = string.sub(res["stdout"], res_start + 1,
				  res_end - string.len("], \"error\":"))
     local ac_error = string.sub(res["stdout"], res_end,
				 string.len(res["stdout"]) - 1)
     local args3 = {
	"curl", "localhost:8765", "-X", "POST",
	"-d", "{\"action\": \"notesInfo\", \"version\": 6, \"params\": { \"notes\": [" .. tostring(ac_result) ..  "] } }",
	"-o", "-"
     }
     local res3 = utils.subprocess({ args = args3 })

     local fields = {}
     local values = {}
     local num_fields = 0
     if (res3["stdout"] ~= 0) then
	local nis = res3["stdout"]

	local _, field_loc = string.find(nis, "fields\": {")
	nis = string.sub(nis, field_loc)

	while (true) do
	   if (string.match(nis, "^ \"modelName")) then break end

	   msg.log("info", "iteration: " .. tostring(num_fields))
	   local _, name_end_pos = string.find(nis, "\":")
	   local field_name = string.sub(nis, string.len(" {\""), name_end_pos - string.len("\":"))
	   fields[num_fields] = field_name

	   local _, value_end_pos = string.find(nis, "%}, ")
	   local value = string.sub(nis, name_end_pos, value_end_pos)
	   value = string.sub(value, string.find(value, "\": ") + string.len("\": \""), string.find(value, "\",") - string.len("\""))
	   values[num_fields] = value

	   num_fields = num_fields + 1

	   msg.log("info", "field name: " .. field_name)
	   msg.log("info", "value: " .. value)
	   nis = string.sub(nis, value_end_pos)
	end
     else
	msg.log("error", "couldn't find most recent note.");
	return;
     end

     require 'pl.pretty'.dump(res3)
     msg.log("info", "beginning append...")

     local i = 0
     local appends = ""
     while (i < num_fields) do
	if (fields[i] == audio_field_name) then
	   values[i] = values[i] .. audio_value
	end

	if (fields[i] == sentence_field_name) then
	   values[i] = values[i] .. sentence_value
	end

	if (fields[i] == picture_field_name) then
	   values[i] = values[i] .. picture_value
	end

	appends = appends .. "\"" .. fields[i] .. "\": \"" .. values[i] .. "\""
	if (i < num_fields - 1) then
	   appends = appends .. ", "
	else
	   appends = appends .. " "
	end

	i = i + 1
     end

     msg.log("info", "appends: " .. appends)

     local args2 = {
	"curl", "localhost:8765", "-X", "POST",
	"-d", "{\"action\": \"updateNoteFields\", \"version\": 6, \"params\": { \"note\": { \"id\": " .. tostring(ac_result) .. ", \"fields\": { " .. appends ..  " } } } }",
	"-o", "-"
     }
     local res2 = utils.subprocess({ args = args2 })
     require 'pl.pretty'.dump(res2)
  else
     msg.log("error", "could not find deck")
  end
end

function print_cur()
   msg.log("info", mp.get_property_native("sub-text"))
end

function make_sentence()
   local sub = ""
   local start_time = mp.get_property_native("time-pos")

   local i = previous_offset
   mp.commandv("sub-seek", tostring(previous_offset))
   while (i < 0) do
      sub = sub .. mp.get_property_native("sub-text")

      i = i + 1
      mp.commandv("sub-seek", "1")
   end

   -- msg.log("info", mp.get_property_native("sub-text"))
   sub = mp.get_property_native("sub-text")
   -- TODO: infinite loop while sub-text is nil
   -- mp.wait_event(1)


   -- mp.wait_event(1)
   while (i < forward_offset) do
      -- mp.wait_event(10)
      -- msg.log("info", mp.get_property_native("sub-text"))
      mp.commandv("sub-seek", tostring(forward_offset))
      -- mp.commandv("seek", "0.6")
      -- sub = sub .. mp.get_property_native("sub-text")

      i = i + 1
      -- mp.commandv("sub-seek", "1")
      -- mp.wait_event(1)
   end

   -- mp.commandv("seek", tostring(start_time), "absolute")
   msg.log("info", "completed")

   mp.set_osd_ass(0, 0, "")
   mp.osd_message("sub: " .. sub, 10)   
end

function get_dir_and_filename()
  local path = mp.get_property("path") or ""
  local filename = mp.get_property("filename/no-ext") or "encode"
  local extension = tostring(string.match(path, "%.([^.]+)$"))
  local dest_dir, _ = utils.split_path(path)
  local contents = utils.readdir(dest_dir)

  if not contents then
    return nil
  end

  return dest_dir, filename
end

function get_dest()
   _, filename = get_dir_and_filename()

  local output = filename .. os.date("%Y%m%d%H%M%S")
  return output
end

function lower_offset()
   mp.set_osd_ass(0, 0, "")
   previous_offset = previous_offset - 1
   mp.osd_message("card offset: " .. tostring(previous_offset), 10)
end

function raise_offset()
   mp.set_osd_ass(0, 0, "")
   forward_offset = forward_offset + 1
   mp.osd_message("card offset: " .. tostring(forward_offset), 10)
end

-- this only needs done once but idgaf
function get_track()
   msg.log("info", "attempt")

   local i = 0
   local tracks_count = mp.get_property_number("track-list/count")
   while i < tracks_count do
     local track_type = mp.get_property(string.format("track-list/%d/type", i))
     local track_index = mp.get_property_number(string.format("track-list/%d/ff-index", i))
     local track_selected = mp.get_property(string.format("track-list/%d/selected", i))

     if (track_type == "audio" and track_selected == "yes") then
       return track_index
     end

     msg.log("info", "checked track " .. i .. " and it wasn't it")
     i = i + 1
   end

   msg.log("info", "no audio track selected?")
   return nil
end

function take_screen()
  mp.commandv("screenshot", "video")

  -- Get the name of the most recent screenshot
  dir = get_dir_and_filename()
  local ls_args = {
     "ls", dir
  }

  local ls_res = utils.subprocess({ args = ls_args, cancellable = false })
  if (ls_res["stdout"] == 0) then
     msg.log("error", "cannot find working directory")
     return
  end

  local default_ss_name = ""
  for line in string.gmatch(ls_res["stdout"], "[^\n]+") do
     default_ss_name = line
  end

  local screen_dest = get_dest() .. ".jpg"
  local mv_args = {
     "mv", default_ss_name, screen_dest
  }
  local res = utils.subprocess({ args = mv_args, cancellable = false })

  local start_pos = mp.get_property_native("sub-start");
  local end_pos = mp.get_property_native("sub-end");
  if (start_pos == nil or end_pos == nil) then
     msg.log("info", "invalid trim")
     return
  end

  return "<img src=\"" .. screen_dest .. "\">"
end

function write_audio()
   local clip_len = end_pos - start_pos
  local source_path = mp.get_property_native("path");
  if (source_path == nil) then
     return
  end

  local track_index = get_track()
  if (track_index == nil) then
     return
  end
  
  local dest = get_dest() .. ".mp3"
  local args = {
    "ffmpeg",
    "-loglevel", "verbose",
    "-hide_banner",

    "-ss", tostring(start_pos),
    "-i", tostring(source_path),
    "-t", tostring(clip_len),

    "-vn",
    "-acodec", "libmp3lame",
    "-ab", "192000",

    "-map", string.format("0:%d", track_index), -- "a:0",

    "-avoid_negative_ts", "make_zero",
    "-async", "1",
    "-strict", "-2",

    dest
  }

  local message = ""
  local res = utils.subprocess({ args = args, cancellable = false })
  
  if (res["status"] ~= 0) then
    if (res["status"] ~= nil) then
  	message = message .. "\nERROR: " .. res["status"]
    end

    if (res["error"] ~= nil) then
  	message = message .. "\nERROR: " .. res["error"]
    end

    if (res["stdout"] ~= nil) then
  	message = message .. "\nstdout: " .. res["stdout"]
    end
    
    msg.log("error", message)
  else
    msg.log("info", "Success: '" .. dest .. "'")
    message = message .. "Done"
    msg.log("info", message)
  end

  return "[sound:" .. dest .. "]"
end

mp.add_key_binding("C", "write_audio", write_audio)
mp.add_key_binding("W", "anki", anki)
