#!/bin/bash
# record audio and copy the file path to the clipboard in a format pasteable to anki.

# kill any subprocesses we create.
cleanup() {
  kill -- -$(jobs -p);
}

# create a temporary wav file and give it a file descriptor in case of crashes
filename_wav=$(mktemp --suffix=.wav);
exec 3>"$filename_wav";

# spawn parecorder in a separate process so we can interface it with dmenu
set -m;
parecord --channels=1 -d alsa_output.pci-0000_00_1b.0.analog-stereo.monitor "${filename_wav}" &

# wait for input
chosen=$(echo -e "copy\nabort" | dmenu -i -p "recording... ");

# go ahead and move this to the anki media folder and copy it's signature to the clipboard.
if [[ "$chosen" == *"copy"* ]]; then
    echo "copying...\n";
    # anki doesn't seem to like wav, so convert it to mp3
    stem="$(date "+%F-%T")"
    lame "${filename_wav}" ~/.local/share/Anki2/User\ 1/collection.media/"${stem}.mp3";

    # copy our anki-formatted file signature to the clipboard.
    echo "[sound:${stem}.mp3]" | xclip -selection clipboard;
fi

# get rid of the wav
rm "${filename_wav}";

# make sure everything gets cleaned up if the program exits
trap cleanup EXIT
