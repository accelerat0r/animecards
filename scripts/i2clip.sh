#!/bin/bash
# select an area to screenshot and put that screenshot in the clipboard.
# note that if you assign this to a hotkey, it might not work unless you specifically
# assign to a hotkey-release event, as scrot -s cancels on a keypress.

scrot -s -e 'xclip -t image/png -selection clipboard -i $f && rm $f'
